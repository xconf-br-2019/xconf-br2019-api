const mongoose = require('mongoose');

const QuestionSchema = new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    card: {
        type: String,
        required: true
    },
    question: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true,
        match: /(ESCALA|USO|ACESSO|ADESIVO|INFO)/
    },
    answers: {
        type: [String],
        required: true
    },
    correct: {
        type: Number,
        min: 0,
        max: 3
    },
    source: {
        type: String
    },
    link: {
        type: String
    }
});

const Question = mongoose.model('Question', QuestionSchema);

Question.getAll = function() {
    return Question.find()
        .select('-_id -__v')
        .lean();
};

module.exports = Question;
