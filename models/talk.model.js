const mongoose = require('mongoose');

const TalkSchema = new mongoose.Schema({
    datetime: {
        type: Date,
        required: true
    },
    speakers: {
        type: [{
            name: { type: String, required: true},
            bio: { type: String, required: true},
            title: { type: String, required: true},
            photo_url: { type: String, required: true}
        }],
        required: true
    },
    title: {
        type: String,
        required: true
    },
    abstract: {
        type: String,
        required: true
    },
    keynote: {
        type: Boolean,
        default: false
    },
    slides: {
        type: String
    }
});

const Talk = mongoose.model('Talk', TalkSchema);

Talk.getAll = function() {
    return Talk.find()
        .select('-_id -__v -speakers._id')
        .sort({ datetime: 1 })
        .lean();
};

module.exports = Talk;
