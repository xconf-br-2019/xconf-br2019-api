const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    answers: {
        type: Map,
        of: [Number],
        default: {}
    }
});

const User = mongoose.model('User', UserSchema);

User.addAnswer = function(mAnswer) {
    return new Promise((resolve, reject) => {
        User.findOne({ name: mAnswer.username }).then((user) => {
            if(user && (!user.answers.has(mAnswer.questionId))) {
                user.answers.set(mAnswer.questionId, [mAnswer.answerIndex, mAnswer.correctIndex]);
                user.save().then((saved) => resolve(saved));
            } else {
                resolve(user);
            }
        }).catch(reject);
    });
};

module.exports = User;
