const express = require('express');

const Question = require('../models/question.model');

module.exports = (app) => {
    const router = express.Router();
    app.use('/questions', router);

    router.get('/', (req, res) => {
        Question.getAll().then((result) => {
            res.status(200).send({
                success: true,
                data: result
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                data: `${err}`
            });
        });
    });
};

module.exports.Question = Question;
