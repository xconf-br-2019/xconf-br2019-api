const express = require('express');

const Talk = require('../models/talk.model');

module.exports = (app) => {
    const router = express.Router();
    app.use('/talks', router);

    router.get('/', (req, res) => {
        Talk.getAll().then((result) => {
            res.status(200).send({
                success: true,
                data: result
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                data: `${err}`
            });
        });
    });
};

module.exports.Talk = Talk;
