const express = require('express');

const User = require('../models/user.model');
const Questions = require('../models/question.model');

module.exports = (app) => {
    const router = express.Router();
    app.use('/users', router);

    router.get('/', (req, res) => {
        res.status(200).send({
            success: true,
            data: { message: 'USERS API OK' }
        });
    });

    router.get('/draw', (req, res) => {
        User.find({}).then((users) => {
            const resCode = (users) ? 200 : 404;

            const ranking = users.sort((a, b) => {
                let aScore = 0;
                let bScore = 0;

                a.answers.forEach((val) => {
                    aScore += (val[1] !== -1) ? 4 * (val[0] === val[1]) : 1;
                });

                b.answers.forEach((val) => {
                    bScore += (val[1] !== -1) ? 4 * (val[0] === val[1]) : 1;
                });

                return bScore - aScore;
            }).map((u) => {
                let s = 0;
                u.answers.forEach((val) => {
                    s += (val[1] !== -1) ? 4 * (val[0] === val[1]) : 1;
                });
                return (u.name + ': ' + s);
            });

            res.status(resCode).send({
                success: true,
                data: ranking || []
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                data: `${err}`
            });
        });
    });

    router.get('/results', (req, res) => {
        const answers = {};
        const questionsById = {};
        let results = '';
        User.find({}).then((users) => {
            users.forEach((u) => {
                u.answers.forEach((a, q) => {
                    if (!(q in answers)) {
                        answers[q] = {};
                    }
                    if (!(a[0] in answers[q])) {
                        answers[q][a[0]] = 0;
                    }
                    answers[q][a[0]] += 1;
                });
            });
            Questions.getAll().then((questions) => {
                questions.forEach((q) => {
                    questionsById[q.id] = { answers: q.answers, question: q.question };
                });

                Object.keys(answers).forEach((qid) => {
                    let row = `${questionsById[qid].question}\n\t`;
                    Object.keys(answers[qid]).forEach((a) => {
                        row += `${questionsById[qid].answers[a]},${answers[qid][a]}\n\t`;
                    });
                    results += row;
                });
                res.status(200).send({
                    success: true,
                    data: { results: results }
                });
            });
        });
    });

    router.get('/:username/exists', (req, res) => {
        User.findOne({ name: req.params.username }).then((user) => {
            res.status(200).send({
                success: true,
                data: { exists: (!!user) }
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                data: `${err}`
            });
        });
    });

    router.get('/:username', (req, res) => {
        User.findOne({ name: req.params.username }).then((user) => {
            const resCode = (user) ? 200 : 404;
            res.status(resCode).send({
                success: true,
                data: user || {}
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                data: `${err}`
            });
        });
    });

    router.post('/:username', (req, res) => {
        User.findOne({ name: req.params.username }).then((user) => {
            user = user || new User({ name: req.params.username });
            user.save().then((saved) => {
                res.status(200).send({
                    success: true,
                    data: saved
                });
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                data: `${err}`
            });
        });
    });

    router.put('/:username', (req, res) => {
        User.addAnswer({
            username: req.params.username,
            questionId: req.body.questionId,
            answerIndex: req.body.answerIndex,
            correctIndex: req.body.correctIndex
        }).then((user) => {
            const resCode = (user) ? 200 : 404;
            res.status(resCode).send({
                success: true,
                data: user || {}
            });
        }).catch((err) => {
            res.status(500).send({
                success: false,
                data: `${err}`
            });
        });
    });
};

module.exports.User = User;
