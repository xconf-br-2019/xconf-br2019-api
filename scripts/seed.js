require('dotenv').config();

const mongoose = require('mongoose');

const logger = require('../utils/logger');

const Talk = require('../models/talk.model');
const Question = require('../models/question.model');

function log(msg) {
    if (require.main === module) {
        logger.info(msg);
    }
}

function seed(dbURI, done, returnCounts) {
    let talks = [];
    let questions = [];
    dbURI = dbURI || process.env.MONGODB_URI_DD;
    returnCounts = returnCounts || (() => {});

    function createTalk(mTalk) {
        return new Promise(function(resolve, reject) {
            const talk = new Talk(mTalk);

            talk.save().then((saved) => {
                log(`saved ${saved.datetime}`);
                resolve(saved);
            }).catch((err) => reject(err));
        });
    }

    function createQuestion(mQuestion) {
        return new Promise(function(resolve, reject) {
            const question = new Question(mQuestion);

            question.save().then((saved) => {
                log(`saved ${saved.id}`);
                resolve(saved);
            }).catch((err) => reject(err));
        });
    }

    mongoose.connect(dbURI, { useNewUrlParser: true })
        .then(() => Talk.deleteMany({ }))
        .then(() => Question.deleteMany({ }))
        .then(() => {
            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T09:15:00-03:00'),
                    title: 'Princípios de Arquitetura Evolutiva',
                    slides: 'https://www.slideshare.net/thoughtworksbr/princpios-de-arquitetura-evolutiva',
                    keynote: true,
                    abstract: 'O ecossistema de desenvolvimento de software está em constante mudança, com um fluxo cada vez mais rápido de inovação em ferramentas, frameworks e técnicas. Nos últimos anos, novas práticas de engenharia de desenvolvimento de software abriram o caminho para repensarmos como a arquitetura de sistemas pode mudar ao longo do tempo de vida de uma aplicação, solução ou negócio. Essa palestra apresenta uma nova maneira de pensar sobre essas mudanças e a evolução de arquiteturas.',
                    speakers: [{
                        name: 'Rebecca Parsons',
                        title: 'CTO, ThoughtWorks',
                        bio: 'Atual CTO da ThoughtWorks, Rebecca conta com décadas de experiência em desenvolvimento de software nos mais diversos setores. Sua experiência técnica inclui liderar a criação de aplicações de objetos distribuídos em grande escala e a integração de sistemas distintos. Além da sua paixão profunda por tecnologia, ela é uma forte defensora da diversidade. Empenhada em aumentar o número de mulheres nos campos da ciência e da tecnologia, Rebecca fez parte do conselho da CodeChix e atua como consultora do Women Who Code.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/rebecca-parsons.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T16:41:00-03:00'),
                    title: 'Introdução à Refatoração',
                    slides: 'https://www.thoughtworks.com/books/refactoring2',
                    keynote: true,
                    abstract: 'A refatoração tornou-se uma habilidade essencial para qualquer desenvolvedor de software. Ela nos permite melhorar a estrutura do código existente em pequenas etapas controladas, minimizando as chances de introduzir erros. Não é apenas importante remover código antigo confuso, como também é essencial ter um código limpo e saudável para facilitar mantê-lo em boa forma. Nessa palestra, usarei o exemplo de abertura do meu livro "Refatoração" para demonstrar a refatoração para aqueles que não conhecem a prática, e para falar sobre os elementos essenciais do uso e do ensino dessa habilidade.',
                    speakers: [{
                        name: 'Martin Fowler',
                        title: 'Chief Scientist, ThoughtWorks',
                        bio: 'Martin Fowler é autor, palestrante e consultor. Seu foco é a  criação de software empresarial e na maximização da produtividade de times de desenvolvimento. Fowler foi pioneiro em várias temáticas como Programação Orientada ao Objeto e Métodos Ágeis. Ele foi um dos autores do Manifesto Ágil e também já escreveu vários livros renomados como "Refactoring", "UML Distilled", "Patterns of Enterprise Application Architecture", e "NoSQL Distilled".',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/martin-fowler.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T10:15:00-03:00'),
                    title: 'Enriquecendo um Modelo de Domínio Anêmico',
                    slides: 'https://www.slideshare.net/thoughtworksbr/enriquecendo-um-modelo-de-domnio-anmico',
                    abstract: 'Pessoas desenvolvedoras tendem a se preocupar mais com os aspectos técnicos de um software. A aproximação da equipe de desenvolvimento com o  negócio pode influenciar na evolução da arquitetura, design e produto. Vamos trazer como exemplo um projeto onde introduzimos técnicas de modelagem baseado em DDD (Domain Driven Design) em um sistema legado que tinha como principal característica um modelo de domínio anêmico. E como chegamos na padronização da arquitetura e criamos uma linguagem única entre pessoas desenvolvedora e especialistas do domínio.',
                    speakers: [{
                        name: 'Gabriela Mattos',
                        title: 'Consultora desenvolvedora, ThoughtWorks Brasil',
                        bio: 'Gabriela Mattos é consultora desenvolvedora de software na ThoughtWorks Brasil, onde atualmente atua em projeto com Java e React. Bacharela em Ciência da Computação pela UFSCar. Engajada na causa de mulheres e pessoas negras na tecnologia, ajudou a fundar os grupos PyLadies São Carlos e Women@Comp e colabora para tornar as oportunidades e espaços tecnológicos com menos vieses.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/gabi-mattos.jpg'
                    }, {
                        name: 'Carlos Maniero',
                        title: 'Consultor de desenvolvimento, ThoughtWorks Brasil',
                        bio: 'Carlos Maniero é consultor de desenvolvimento de software ágil. Acredita que a aproximação entre a equipe de desenvolvimento e stakeholders é uma oportunidade única para a integração da tecnologia com o negócio e a chave para a construção de produtos ambiciosos.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/carlos-maniero.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T14:20:00-03:00'),
                    title: 'Integração entre design e tecnologia',
                    slides: 'https://www.slideshare.net/thoughtworksbr/integrao-entre-design-e-tecnologia',
                    abstract: 'O que é o design e como ele se aproxima das pessoas. Como o design atua com os pilares de pessoas, negócios e inovação para entregar uma experiência humana integrando design com tecnologia.',
                    speakers: [{
                        name: 'Rodrigo Cruz',
                        title: 'Head of Design, ThoughtWorks Brasil',
                        bio: 'Rodrigo possui graduação em Desenho industrial e Projeto de Produto pela UFPE e MBA em Gestão Estratégica de pessoas. Atualmente, é Head de Design na ThoughtWorks Brasil. Tem ampla experiência em desenvolvimento 3D para Mobile, já atuou como líder de equipes de design, e hoje atua como designer de interação com foco em usabilidade e agilidade.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/rodrigo-cruz.jpg'
                    }, {
                        name: 'Tathiane Mendonça',
                        title: 'Líder de Oferta e Inovação de Produto, ThoughtWorks Brasil',
                        bio: 'Estrategista de experiência e líder de Oferta e Inovação de Produto na ThoughtWorks Brasil. Mais de 9 anos de experiência em UX com background em Interação Humano-Computador (IHC). Atuação em diferentes segmentos de mercado com foco em User Research e Service Design.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/tathi-mendonca.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T17:41:00-03:00'),
                    title: 'Mundo Mobile: o que temos usado e o que vem por aí',
                    slides: 'https://www.slideshare.net/thoughtworksbr/mundo-mobile-o-que-temos-usado-e-o-que-vem-por-a-164096945',
                    abstract: 'O impacto do uso de apps mobile é visível em todas organizações. A maioria das pessoas e times tem conhecimento que mais da metade do uso de internet vem de mobile, e que o mundo dos apps também foi responsável em fazer surgir o primeiro unicórnio brasileiro (a 99 foi avaliada em 1 bilhão). Apesar de muitas empresas estarem adotando a estratégia mobile-first, ainda existem dificuldade em saber quais são as tecnologias mobile mais adequadas no contexto de cada organização, e como podemos acompanhar as tendências de mercado. Para ajudar nesses pontos e trazer mais contexto para desenvolvimento de projetos, vou compartilhar alguns resultados recentes e marcos importantes atingidos pelos apps no Brasil e falar sobre o retrato do desenvolvimento mobile no Brasil e o que temos usados nos projetos na ThoughtWorks Brasil.',
                    speakers: [{
                        name: 'Juliana Chahoud',
                        title: 'Desenvolvedora, ThoughtWorks Brasil',
                        bio: 'Juliana Chahoud é Consultora Principal de Desenvolvimento da ThoughtWorks e foi professora no MBA em Desenvolvimento de Aplicativos e Games na FIAP. Trabalhou anteriormente como Engenheira de Soluções no Twitter e foi líder técnica em times de desenvolvimento de aplicativos como PlayKids, 99 Taxis e ZAP Imoveis. Possui mestrado em Inteligência Artificial pela USP e atua há vinte anos na área de TI em Desenvolvimento de Sistemas.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/juliana-chahoud.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T11:29:00-03:00'),
                    title: 'Desenvolvimento para Big-Screen - fragmentação = entropia infinita?',
                    slides: 'https://www.slideshare.net/thoughtworksbr/desenvolvimento-para-bigscreen-fragmentao-entropia-infinita',
                    abstract: 'Já tem alguns anos que um aparelho de TV faz muito mais do que simplesmente sintonizar ondas de rádio e exibir uma imagem a partir do sinal eletromagnético capturado. Quem um dia já imaginou que os nossos aparelhos de TV teriam um sistema operacional embarcado, com engines javascript, bugs de segurança, problemas com SSL e limitações de processamento e memória RAM? Num mundo onde computadores de mesa, servidores e dispositivos móveis tendem a ter capacidade infinita de memória RAM e processamento, o mundo IoT é uma viagem ao passado, um passado onde a citação do Bill Gates - que "640KB de RAM devem ser suficientes para qualquer pessoa" - se torna quase um dogma. Num ambiente extremamente fragmentado, onde os fabricantes brigam por centavos no custo unitário de produção, e tomam decisões fragmentadas e localmente otimizadas, tendemos a fragmentação absoluta e entropia infinita. Há salvação?',
                    speakers: [{
                        name: 'Igor Macaubas',
                        title: 'Head de plataformas de video online, Globo.com',
                        bio: 'Engenheiro de software por formação, Igor enveredou para a área de gestão de projetos e produtos. Fez dois MBAs de Gestão executiva (IBMEC e COPPEAD) e hoje lidera toda estratégia e o desenvolvimento da plataforma de vídeos online do Grupo Globo, plataforma que entrega mais de 400MM de visualizações de vídeo por mês para quase 15MM de usuários únicos, em mais de 20 tipos de dispositivos diferentes. Lidera equipes incríveis, que constroem soluções de APIs, Players multiscreen, CDNs para entrega de vídeo, etc.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/igor-macaubas.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T18:03:00-03:00'),
                    title: 'Entrega Contínua aplicada a Machine Learning',
                    slides: 'https://www.slideshare.net/thoughtworksbr/entrega-contnua-aplicada-a-machine-learning',
                    abstract: 'A entrega contínua para modelos de aprendizado de máquina (CD4ML) aplica as práticas de entrega contínua ao desenvolvimento de modelos de aprendizado de máquina para que estes estejam sempre prontos para produção. Essa técnica soluciona dois problemas principais do modelo tradicional de aprendizado de máquina: o tempo longo de ciclo entre treinar modelos e implementá-los em produção, e o uso em produção de modelos treinados com dados obsoletos. Nessa palestra vamos falar sobre práticas usadas para criar pipelines de entrega contínua que sejam adaptáveis à mudanças na estrutura de modelos e mudanças no conjunto de dados de treinamento e testes.',
                    speakers: [{
                        name: 'Adriana Hoher',
                        title: 'Desenvolvedora consultora',
                        bio: 'Adriana é desenvolvedora e consultora na ThoughtWorks Brasil e apaixonada por música e tudo que os dados podem nos dizer. Economista de formação e aluna especial do Mestrado em Estatística pela UFRGS, analista de dados por vocação, feminista e cat lover. Esteve envolvida no último ano em projetos de Machine Learning e tuning de algoritmos supervisionados para o maior conglomerado de mídia da América Latina.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/adriana-hoher.jpg'
                    }, {
                        name: 'Andherson Maeda',
                        title: 'Engenheiro de machine learning',
                        bio: 'Apaixonado por Machine Learning, Inteligência Artificial e guitarrista nas horas vagas. Já se aventurou por aí em quase tudo na TI, desde infraestrutura com cloud privada, integração contínua, arquitetura de software e liderança de equipes ágeis. Atuando como engenheiro de machine learning, participa de iniciativas de inteligência artificial e aplica algoritmos de aprendizagem de máquina para gerar valor à sociedade.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/andherson-maeda.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T15:04:00-03:00'),
                    title: 'DevOps e Plataformas Digitais de A a Z',
                    slides: 'https://www.slideshare.net/thoughtworksbr/devops-e-plataformas-digitais-de-a-a-z',
                    abstract: ' A transformação digital e a nuvem popularizaram ainda mais os termos Plataforma e DevOps. Atualmente, ambas as palavras são amplamente usadas para se referir a coisas muito diferentes. Por exemplo, vemos a palavra plataforma relacionada a provedores de nuvem e plataformas como modelo de negócios. Similarmente, a palavra DevOps tem sido usada para se referir a novos papéis ou perfis e tecnologias relacionadas à automação de infraestrutura. Acreditamos que é necessário estabelecer uma linguagem comum que nos permita levar as organizações um passo mais perto da era digital. Nesta palestra, abordaremos os conceitos de “Plataforma Digital” e “Cultura DevOps” no contexto da popular e desejada Transformação Digital.',
                    speakers: [{
                        name: 'Luisa Emme',
                        title: 'Diretora de Tecnologia, ThoughtWorks Equador',
                        bio: 'Diretora de Tecnologia da ThoughWorks Equador, faz parte da empresa há 4 anos e sua contribuição tem sido fundamental para o crescimento do escritório de Quito nos últimos anos. Durante sua carreira, já participou em vários eventos de tecnologia, onde compartilhou seu aprendizado e experiências em Arquiteturas Evolutivas, Plataformas Digitais e DevOps. Participa ativamente de iniciativas relacionadas à diversidade de gênero na indústria de tecnologia, sendo mentora em eventos para mulheres como Rails Girls e Django Girls.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/luisa-emme.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T14:05:00-03:00'),
                    title: 'Como a colaboração entre artistas e engenheiros fomenta a pesquisa de tecnologias emergentes',
                    slides: 'https://www.slideshare.net/thoughtworksbr/como-a-colaborao-entre-artistas-e-engenheiros-fomenta-a-pesquisa-de-tecnologias-emergentes',
                    abstract: 'No ritmo em que nossa tecnologia muda, novas formas de colaboração são necessárias para investigar e reimaginar o impacto dessas tecnologias emergentes na cultura e na sociedade. Na ThoughtWorks, nós incubamos artistas para resolver esses problemas.',
                    speakers: [{
                        name: 'Julien Deswaef',
                        title: 'Artista/Experience Designer, ThoughtWorks Arts',
                        bio: 'Julien Deswaef é um artista, designer de experiência e desenvolvedor de interfaces, atualmente trabalhando na ThoughtWorks Arts. Ativo tanto em artes visuais quanto no movimento de software livre, ele colabora ativamente em projetos de arte e tecnologia.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/julien-deswaef.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T14:42:00-03:00'),
                    title: 'Quebrando silos por meio de uma cultura de produto',
                    slides: 'https://www.slideshare.net/thoughtworksbr/quebrando-silos-por-meio-de-uma-cultura-de-produto',
                    abstract: 'A LATAM Digital começou em 2013, usando metodologias ágeis, e rapidamente expandiu para mais de 40 equipes e 500 pessoas. Com esse crescimento rápido, foram formados silos de conhecimento e atuação, trazendo efeitos colaterais na forma em como produtos são criados. Vamos compartilhar com você nossa jornada e como estamos tentando romper os silos com uma Cultura de Produto.',
                    speakers: [{
                        name: 'Vanesa Tejada',
                        title: 'Diretora de entrega de produtos, LATAM Digital',
                        bio: 'Atual diretora de entrega de produtos na LATAM Digital, sua principal contribuição é o desenvolvimento de uma cultura para criar produtos com base em aprendizado contínuo, gestão e desenvolvimento eficazes e equipes colaborativas. Conta com ampla experiência com modelos de Lean Startup, OKRs, Metodologias Lean-Agile, e Gestão Visual de Produtividade Pessoal (GTD) para criar uma visão comum em times de projeto, garantir colaboração e maximizar resultados.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/vanesa-tejada.jpg'
                    }, {
                        name: 'Luis Mizutani',
                        title: 'Analista de negócios, ThoughtWorks Brasil',
                        bio: 'Luis Mizutani trabalha há mais de 10 anos liderando equipes de produtos locais e distribuídos, dentro do Brasil e em mercados internacionais. Durante sua carreira, já trabalhou para uma variedade de indústrias, incluindo mídia, notícias, classificados on-line, sistemas de busca, comércio eletrônico, turismo, jogos, saúde e companhias aéreas. Ele já teve vários papéis na ThoughtWorks e, nos últimos 4 anos, gerencia equipes de produtos da ThoughtWorks para a LATAM Digital.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/luis-mizutani.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T11:51:00-03:00'),
                    title: 'Pessoas > Linguagens',
                    slides: 'https://www.slideshare.net/thoughtworksbr/pessoas-linguagens-164109569',
                    abstract: 'Pela teoria da completude de Turing, as linguagens de programação se equivalem. Então, porque não fazer tudo em Java, JavaScript, ou C#? Quando a escolha de uma linguagem diferente das mais comuns pode fazer sentido? Além de fatores técnicos, quais fatores humanos podem ser relevantes na escolha de uma linguagem de programação para o próximo produto da sua empresa?',
                    speakers: [{
                        name: 'Luciano Ramalho',
                        title: 'Consultor de desenvolvimento, ThoughtWorks Brasil',
                        bio: 'Luciano Ramalho é autor do livro "Fluent Python", publicado em 9 idiomas. Nos últimos anos, coordenou a trilha "Linguagens do Século 21" da conferência QCon SP. Na ThoughtWorks, Luciano contribui com iniciativas de aprendizagem e onboarding em temas como novas linguagens e sistemas distribuídos.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/luciano-ramalho.jpg'
                    }, {
                        name: 'Rosi Teixeira',
                        title: 'Consultora de desenvolvimento, ThoughtWorks Brasil',
                        bio: 'Rosi é tecnologista, trabalhando na área de TI há mais de 20 anos, sempre na área de desenvolvimento de software, tendo ocupado nestes anos vários papéis como desenvolvedora de software, tech lead, gerente mas sempre, sempre codando. Amante e patrocinadora do trabalho em equipe, acredita na força do coletivo e como pessoas diferentes podem construir coisas incríveis juntas. É com esse espírito que trabalha para fazer dos produtos e serviços que entrega, elementos transformadores dos negócios e da sociedade.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/rosi-teixeira.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T11:07:00-03:00'),
                    title: 'Padrão de estrangulamento na prática – A jornada de modernização de um legado Perl de 20 anos',
                    slides: 'https://www.slideshare.net/thoughtworksbr/padro-de-estrangulamento-na-prtica-a-jornada-de-modernizao-de-um-legado-perl-de-20-anos',
                    abstract: 'Não há pesadelo maior para pessoas desenvolvedoras que manter e dar suporte a um sistema legado. Nós podíamos ter vivido isso com um sistema de 20 anos, escrito em Perl, sem cobertura efetiva de testes, mas de extrema relevância: ele é o coração do negócio – por lá passam 100 mil transações diárias. Optamos por modernizar a aplicação. Venha saber mais sobre a execução dessa estratégia e benefícios colhidos.',
                    speakers: [{
                        name: 'Thalita Gomes',
                        title: 'Consultora de desenvolvimento, ThoughtWorks Brasil',
                        bio: 'Thalita lidera a equipe da Thoughtworks que trabalha no case a ser apresentado e é a responsável pela definição da estratégia de migração, da qualidade da entrega e do desenvolvimento do time.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/thalita-gomes.jpg'
                    }, {
                        name: 'Barbara Sanches',
                        title: 'Analista de negócios, ThoughtWorks Brasil',
                        bio: 'Bárbara atualmente atua em iniciativas de migração de sistemas legados e de métricas de negócios pela Thoughtworks. Ela liderou, a nível de negócio, o case em questão.',
                        photo_url: 'https://xconf-br2019.s3.amazonaws.com/barbara-sanches.jpg'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T09:05:00-03:00'),
                    title: 'Abertura',
                    abstract: ' ',
                    speakers: [{
                        name: 'Caroline Cintra',
                        title: 'Diretora-Presidente, ThoughtWorks Brasil',
                        bio: '...',
                        photo_url: 'https://'
                    }, {
                        name: 'Alexey Boas',
                        title: 'Diretor de Tecnologia, ThoughtWorks Brasil',
                        bio: '...',
                        photo_url: 'https://'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T08:00:00-03:00'),
                    title: 'Café da Manhã',
                    abstract: ' ',
                    speakers: [{
                        name: ' ',
                        title: ' ',
                        bio: ' ',
                        photo_url: ' '
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T12:28:00-03:00'),
                    title: 'Almoço',
                    abstract: ' ',
                    speakers: [{
                        name: ' ',
                        title: ' ',
                        bio: ' ',
                        photo_url: ' '
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T15:34:00-03:00'),
                    title: 'Coffee Break',
                    abstract: ' ',
                    speakers: [{
                        name: ' ',
                        title: ' ',
                        bio: ' ',
                        photo_url: ' '
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T18:54:00-03:00'),
                    title: 'Happy Hour',
                    abstract: ' ',
                    speakers: [{
                        name: ' ',
                        title: ' ',
                        bio: ' ',
                        photo_url: ' '
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T18:39:00-03:00'),
                    title: 'Encerramento',
                    abstract: ' ',
                    speakers: [{
                        name: 'Caroline Cintra',
                        title: 'Diretora-Presidente, ThoughtWorks Brasil',
                        bio: '...',
                        photo_url: 'https://'
                    }, {
                        name: 'Alexey Boas',
                        title: 'Diretor de Tecnologia, ThoughtWorks Brasil',
                        bio: '...',
                        photo_url: 'https://'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T08:55:00-03:00'),
                    title: 'Abertura Musical',
                    abstract: ' ',
                    speakers: [{
                        name: 'Nath Rodrigues',
                        title: 'Musicista',
                        bio: '...',
                        photo_url: 'https://'
                    }, {
                        name: 'Pat Manoese',
                        title: 'Musicista',
                        bio: '...',
                        photo_url: 'https://'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T13:58:00-03:00'),
                    title: 'Intervenção Musical',
                    abstract: ' ',
                    speakers: [{
                        name: 'Nath Rodrigues',
                        title: 'Musicista',
                        bio: '...',
                        photo_url: 'https://'
                    }, {
                        name: 'Pat Manoese',
                        title: 'Musicista',
                        bio: '...',
                        photo_url: 'https://'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T18:35:00-03:00'),
                    title: 'Fechamento Musical',
                    abstract: ' ',
                    speakers: [{
                        name: 'Nath Rodrigues',
                        title: 'Musicista',
                        bio: '...',
                        photo_url: 'https://'
                    }, {
                        name: 'Pat Manoese',
                        title: 'Musicista',
                        bio: '...',
                        photo_url: 'https://'
                    }]
                }));

            talks.push(
                createTalk({
                    datetime: new Date('2019-08-15T10:37:00-03:00'),
                    title: 'Intervalo',
                    abstract: ' ',
                    speakers: [{
                        name: ' ',
                        title: ' ',
                        bio: '...',
                        photo_url: 'https://'
                    }]
                }));

        })
        .then(() => {
            questions.push(
                createQuestion({
                    id: 'ESCANDALO',
                    card: 'O Escândalo',
                    category: 'ESCALA',
                    question: 'Você já avaliou as possíveis manchetes negativas sobre o software que você esteve desenvolvendo?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'SO-SUCESSO',
                    card: 'Só Sucesso',
                    category: 'ESCALA',
                    question: 'Você já criou um plano de ação para quando existirem milhões de pessoas usando um software que você desenvolveu?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'MAE-NATUREZA',
                    card: 'Mãe Natureza',
                    category: 'ESCALA',
                    question: 'Você já avaliou os possíveis impactos que um software que você desenvolveu podem causar no meio ambiente?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'ESTRELA-RADIO',
                    card: 'A Estrela do Rádio',
                    category: 'ESCALA',
                    question: 'Você já avaliou as possíveis profissões ou serviços que podem desaparecer com o software que você esteve envolvido?',
                    answers: ['SIM', 'NÃO']
                })
            );

            questions.push(
                createQuestion({
                    id: 'SEREIA',
                    card: 'A Sereia',
                    category: 'USO',
                    question: 'Você já realizou ações para evitar possíveis problemas causados pelo uso excessivo do software que você desenvolveu?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'BFFS',
                    card: 'As BFFs',
                    category: 'USO',
                    question: 'Você já avaliou como um software que você desenvolveu pôde melhorar ou piorar o relacionamento entre as pessoas que o utilizam?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'SUPERFA',
                    card: 'O Superfã',
                    category: 'USO',
                    question: 'Você já criou algum plano de ação para possíveis comportamentos excessivamente apaixonados e prejudiciais a seus usuários?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'LOBO-MAU',
                    card: 'O Lobo Mau',
                    category: 'USO',
                    question: 'Você já realizou alguma interferência no software que você desenvolveu para evitar o uso por pessoas má intencionadas?',
                    answers: ['SIM', 'NÃO']
                })
            );

            questions.push(
                createQuestion({
                    id: 'ESQUECIDO',
                    card: 'O Esquecido',
                    category: 'ACESSO',
                    question: 'Você já excluiu ou restringiu o uso do seu software, baseando-se em fatores socioeconômicos como classe, renda ou região?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'CAO-SERVICO',
                    card: 'O Cão de Serviço',
                    category: 'ACESSO',
                    question: 'Você já avaliou maneiras do seu software empoderar a vida de pessoas menos favorecidas?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'CATALISADOR',
                    card: 'O Catalisador',
                    category: 'ACESSO',
                    question: 'Você já considerou como seu software pode influenciar a mudança de hábitos culturais?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'TRAIDOR',
                    card: 'O Traidor',
                    category: 'ACESSO',
                    question: 'Você já mapeou possíveis maneiras das pessoas perderem a confiança em seu software?',
                    answers: ['SIM', 'NÃO']
                })
            );
            questions.push(
                createQuestion({
                    id: 'ACESSO',
                    card: 'Equidade + Acesso',
                    category: 'ZADESIVO',
                    question: 'Quantas HORAS por dia um ciclista que faz entregas por aplicativo precisa trabalhar para ganhar um salário mínimo mensal (R$998,00)?',
                    answers: ['6', '8', '10', '12'],
                    correct: 3
                })
            );
            questions.push(
                createQuestion({
                    id: 'USO',
                    card: 'Uso + Usabilidade',
                    category: 'ZADESIVO',
                    question: 'Qual REDE SOCIAL foi avaliada como a mais prejudicial à saúde mental dos jovens?',
                    answers: ['Instagram', 'Facebook', 'Twitter', 'WhatsApp'],
                    correct: 0
                })
            );
            questions.push(
                createQuestion({
                    id: 'ESCALA',
                    card: 'Escala + Disrupção',
                    category: 'ZADESIVO',
                    question: 'Quantas TONELADAS de lixo eletrônico são produzidas anualmente pela indústria de tecnologia?',
                    answers: ['11', '21', '41', '81'],
                    correct: 2
                })
            );

            questions.push(
                createQuestion({
                    id: 'ACESSO_INFO',
                    card: 'Jornada maior que 24 horas e um salário menor que o mínimo, essa é a vida dos ciclistas de aplicativo em SP',
                    category: 'INFO',
                    question: 'Fonte: El País',
                    source: 'El País',
                    link: 'https://brasil.elpais.com/brasil/2019/08/06/politica/1565115205_330204.html',
                    answers: ['LER MAIS', 'FECHAR']
                })
            );
            questions.push(
                createQuestion({
                    id: 'USO_INFO',
                    card: 'Estudo aponta que Instagram é a rede social mais nociva à saúde mental',
                    category: 'INFO',
                    question: 'Fonte: Super Interessante',
                    source: 'Super Interessante',
                    link: 'https://super.abril.com.br/sociedade/instagram-e-a-rede-social-mais-prejudicial-a-saude-mental/',
                    answers: ['LER MAIS', 'FECHAR']
                })
            );
            questions.push(
                createQuestion({
                    id: 'ESCALA_INFO',
                    card: 'Obsolescência programada gera acúmulo de lixo pelo mundo',
                    category: 'INFO',
                    question: 'Fonte: Canaltech',
                    source: 'Canaltech',
                    link: 'https://canaltech.com.br/produtos/uma-analise-da-obsolescencia-programada-e-o-acumulo-de-lixo-eletronico-no-mundo-102156/',
                    answers: ['LER MAIS', 'FECHAR']
                })
            );

        })
        .then(() => {
            returnCounts({
                talks: talks.length,
                questions: questions.length
            });

            Promise.all(talks.concat(questions)).then(() => {
                log('done');
                if (require.main === module) {
                    mongoose.disconnect();
                }
                if(done) done();
            });
        }).catch((err) => {
            logger.error('while saving slots: ' + err);
        });
}

module.exports = seed;

if (require.main === module) {
    seed();
}
