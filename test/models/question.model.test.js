const chai = require('chai');
chai.should();

const mongoose = require('mongoose');

const logger = require('../../utils/logger');

const Question = require('../../models/question.model');

const seed = require('../../scripts/seed');

describe('Question Model', () => {
    let TOTAL_QUESTIONS;

    before((done) => {
        const dbURI = `${process.env.MONGODB_URI_DD}-test`;

        mongoose.connect(dbURI, { useNewUrlParser: true })
            .then(() => {
                seed(dbURI, done, (t) => TOTAL_QUESTIONS = t.questions);
            }).catch((err) => {
                logger.error('Not connected to db: ' + err);
                process.exit(1);
            });
    });

    after((done) => {
        mongoose.connection.dropDatabase().then(() => {
            done();
        }).catch((err) => {
            logger.error('Not connected to db: ' + err);
            process.exit(1);
        });
    });

    it('there should be seeded questions', () => {
        TOTAL_QUESTIONS.should.be.above(0);
    });

    it('.getAll() should return all questions', () => {
        return Question.getAll().then((result) => {
            result.length.should.be.equal(TOTAL_QUESTIONS);
        });
    });

    it('all seeded questions should have required fields', () => {
        return Question.getAll().then((result) => {
            result.forEach((question) => {
                question.should.include.all.keys('id', 'card', 'question', 'answers', 'category');
                question.answers.length.should.be.above(0);
            });
        });
    });
});
