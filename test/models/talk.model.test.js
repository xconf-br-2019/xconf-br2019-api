const chai = require('chai');
chai.should();

const mongoose = require('mongoose');

const logger = require('../../utils/logger');

const Talk = require('../../models/talk.model');

const seed = require('../../scripts/seed');

describe('Talk Model', () => {
    let TOTAL_TALKS;

    before((done) => {
        const dbURI = `${process.env.MONGODB_URI_DD}-test`;

        mongoose.connect(dbURI, { useNewUrlParser: true })
            .then(() => {
                seed(dbURI, done, (t) => TOTAL_TALKS = t.talks);
            }).catch((err) => {
                logger.error('Not connected to db: ' + err);
                process.exit(1);
            });
    });

    after((done) => {
        mongoose.connection.dropDatabase().then(() => {
            done();
        }).catch((err) => {
            logger.error('Not connected to db: ' + err);
            process.exit(1);
        });
    });

    it('there should be seeded talks', () => {
        TOTAL_TALKS.should.be.above(0);
    });

    it('.getAll() should return all seeded talks', () => {
        return Talk.getAll().then((result) => {
            result.length.should.be.equal(TOTAL_TALKS);
        });
    });

    it('all seeded talks should have required fields', () => {
        return Talk.getAll().then((result) => {
            result.forEach((talk) => {
                talk.should.include.all.keys('datetime', 'speakers', 'title', 'abstract', 'keynote');
                talk.speakers.forEach((speaker) => {
                    speaker.should.include.all.keys('name', 'title', 'bio', 'photo_url');
                });
            });
        });
    });
});
