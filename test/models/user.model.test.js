const spy = require('sinon').spy;
const chai = require('chai');
chai.should();

const mongoose = require('mongoose');

const logger = require('../../utils/logger');

const User = require('../../models/user.model');

describe('User Model', () => {
    const USER_NAMES = ['thiago', 'philadelphia', 'jane'];
    const FAKE_NAMES = ['avjas', 'ASGHJ', '!@#EWDF'];
    const ONE_ANSWER = { username: USER_NAMES[0], questionId: 'GDPR', answerIndex: 2, correctIndex: -1 };
    const NEW_ANSWER = { username: USER_NAMES[0], questionId: 'GDPR', answerIndex: 4, correctIndex: -1 };

    before((done) => {
        const dbURI = `${process.env.MONGODB_URI_DD}-test`;

        mongoose.connect(dbURI, { useNewUrlParser: true })
            .then(() => {
                const userPromises = [];
                USER_NAMES.forEach((uname) => {
                    userPromises.push(new Promise((resolve, reject) => {
                        const user = new User({ name: uname });
                        user.save().then((saved) => {
                            resolve(saved);
                        }).catch(reject);
                    }));
                });

                Promise.all(userPromises).then(() => {
                    done();
                });
            }).catch((err) => {
                logger.error('Not connected to db: ' + err);
                process.exit(1);
            });
    });

    after((done) => {
        mongoose.connection.dropDatabase().then(() => {
            done();
        }).catch((err) => {
            logger.error('Not connected to db: ' + err);
            process.exit(1);
        });
    });

    it('.addAnswer() should add answer to new question', () => {
        const save = spy(User.prototype, 'save');
        return User.addAnswer(ONE_ANSWER).then((result) => {
            result.answers.get(ONE_ANSWER.questionId)[0].should.equal(ONE_ANSWER.answerIndex);
            save.calledOnce.should.be.true;
            save.restore();
        });
    });

    it('.addAnswer() should not add answer to already answered question', () => {
        const save = spy(User.prototype, 'save');
        return User.addAnswer(ONE_ANSWER).then((result) => {
            User.addAnswer(NEW_ANSWER).then((newResult) => {
                newResult.answers.get(NEW_ANSWER.questionId)[0].should.equal(ONE_ANSWER.answerIndex);
                save.called.should.be.false;
                save.restore();
            });
        });
    });
});
