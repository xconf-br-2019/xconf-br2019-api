const stub = require('sinon').stub;
const chai = require('chai');
chai.use(require('chai-http'));

const express = require('express');
const app = express();

const question = require('../../routes/question.route');
const someQuestions = require('../fixtures/question.fixture.json').questions;

describe('Question Routes', () => {
    question(app);

    it('GET /questions/ should return 2 questions', (done) => {
        const getAll = stub(question.Question, 'getAll').resolves(someQuestions);

        chai.request(app).get('/questions').end((err, res) => {
            res.should.have.status(200);
            res.body.success.should.be.true;
            res.body.data.length.should.be.equal(2);
            getAll.calledOnce.should.be.true;

            res.body.data.forEach((question) => {
                question.should.include.all.keys('id', 'card', 'question', 'category', 'answers');
                question.answers.length.should.be.above(0);
            });

            getAll.restore();
            done();
        });
    });
});
