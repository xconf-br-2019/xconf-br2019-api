const stub = require('sinon').stub;
const chai = require('chai');
chai.use(require('chai-http'));

const express = require('express');
const app = express();

const talk = require('../../routes/talk.route');
const someTalks = require('../fixtures/talk.fixture.json').talks;

describe('Talk Routes', () => {
    talk(app);

    it('GET /talks/ should return 3 talks', (done) => {
        const getAll = stub(talk.Talk, 'getAll').resolves(someTalks);

        chai.request(app).get('/talks').end((err, res) => {
            res.should.have.status(200);
            res.body.success.should.be.true;
            res.body.data.length.should.be.equal(3);
            getAll.calledOnce.should.be.true;

            res.body.data.forEach((talk) => {
                talk.should.include.all.keys('datetime', 'title', 'abstract', 'speakers');
                talk.speakers.forEach((speaker) => {
                    speaker.should.include.all.keys('name', 'title', 'bio', 'photo_url');
                });
            });

            getAll.restore();
            done();
        });
    });
});
