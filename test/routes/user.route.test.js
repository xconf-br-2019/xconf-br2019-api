const stub = require('sinon').stub;
const chai = require('chai');
chai.use(require('chai-http'));

const express = require('express');
const app = express();
app.use(express.json({ limit: '5mb' }));

const user = require('../../routes/user.route');
const someUsers = require('../fixtures/user.fixture.json').users;

describe('User Routes', () => {
    user(app);

    it('GET /users/username should return 200 when user exists', (done) => {
        const findOne = stub(user.User, 'findOne').resolves(someUsers[1]);

        chai.request(app).get('/users/' + someUsers[1].name).end((err, res) => {
            res.should.have.status(200);
            res.body.success.should.be.true;
            res.body.data.should.include.all.keys('name', 'answers');
            findOne.calledOnce.should.be.true;

            findOne.restore();
            done();
        });
    });

    it('GET /users/username should return 404 when user doesn\'t exist', (done) => {
        const findOne = stub(user.User, 'findOne').resolves(null);

        chai.request(app).get('/users/' + someUsers[1].name).end((err, res) => {
            res.should.have.status(404);
            res.body.success.should.be.true;
            res.body.data.should.be.empty;
            findOne.calledOnce.should.be.true;

            findOne.restore();
            done();
        });
    });

    it('POST /users/username should create new user when user doesn\'t exist', (done) => {
        const findOne = stub(user.User, 'findOne').resolves(null);
        const save = stub(user.User.prototype, 'save').resolves(someUsers[0]);

        chai.request(app).post('/users/' + someUsers[0].name).end((err, res) => {
            res.should.have.status(200);
            res.body.success.should.be.true;
            res.body.data.should.include.all.keys('name', 'answers');
            res.body.data.name.should.equal(someUsers[0].name);
            findOne.calledOnce.should.be.true;
            save.calledOnce.should.be.true;

            save.restore();
            findOne.restore();
            done();
        });
    });

    it('POST /users/username should return user when user already exists', (done) => {
        const findOne = stub(user.User, 'findOne').resolves(new user.User({ name: someUsers[0].name }));
        const save = stub(user.User.prototype, 'save').resolves(someUsers[0]);

        chai.request(app).post('/users/' + someUsers[0].name).end((err, res) => {
            res.should.have.status(200);
            res.body.success.should.be.true;
            res.body.data.should.include.all.keys('name', 'answers');
            res.body.data.name.should.equal(someUsers[0].name);
            findOne.calledOnce.should.be.true;
            save.calledOnce.should.be.true;

            save.restore();
            findOne.restore();
            done();
        });
    });

    it('PUT /users/username should add answer when user exists', (done) => {
        const addAnswer = stub(user.User, 'addAnswer').resolves(new user.User(someUsers[2]));

        const sendObject = {
            questionId: Object.keys(someUsers[2].answers)[0],
            answerIndex: someUsers[2].answers[Object.keys(someUsers[2].answers)[0]]
        };

        chai.request(app).put('/users/' + someUsers[2].name)
            .set('content-type', 'application/json')
            .send(sendObject).end((err, res) => {
            res.should.have.status(200);
            res.body.success.should.be.true;
            res.body.data.should.include.all.keys('name', 'answers');
            res.body.data.name.should.equal(someUsers[2].name);
            res.body.data.answers.should.eql(someUsers[2].answers);
            addAnswer.calledOnce.should.be.true;

            addAnswer.restore();
            done();
        });
    });

    it('PUT /users/username should not add answer when user doesn\'t exist', (done) => {
        const addAnswer = stub(user.User, 'addAnswer').resolves(null);

        const sendObject = {
            questionId: Object.keys(someUsers[2].answers)[0],
            answerIndex: someUsers[2].answers[Object.keys(someUsers[2].answers)[0]]
        };

        chai.request(app).put('/users/' + someUsers[2].name)
            .set('content-type', 'application/json')
            .send(sendObject).end((err, res) => {
            res.should.have.status(404);
            res.body.success.should.be.true;
            res.body.data.should.be.empty;
            addAnswer.calledOnce.should.be.true;

            addAnswer.restore();
            done();
        });
    });
});
